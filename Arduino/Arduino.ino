#define frontRight_dirPin 2
#define frontRight_stepPin 3

#define frontLeft_dirPin 5
#define frontLeft_stepPin 6

#define rearRight_dirPin 8
#define rearRight_stepPin 9

#define rearLeft_dirPin 11
#define rearLeft_stepPin 12

int timing = 50;

String input = "99";
int state = 7;

void setup() 
{

  Serial.begin(115200); 
  delay(2000);  
 
  Serial.println("Type something!");
    
  pinMode(frontRight_stepPin, OUTPUT);
  pinMode(frontRight_dirPin, OUTPUT);

  pinMode(frontLeft_stepPin, OUTPUT);
  pinMode(frontLeft_dirPin, OUTPUT);

  pinMode(rearRight_stepPin, OUTPUT);
  pinMode(rearRight_dirPin, OUTPUT);

  pinMode(rearLeft_stepPin, OUTPUT);
  pinMode(rearLeft_dirPin, OUTPUT);
}

void loop() 
{
  if(Serial.available())
  {
    input = Serial.read();
    if(input == "122")
    {
      Serial.println("1");
      state = 1;
    }
    if(input =="115")
    {
      Serial.println("2");
      state = 2;
    }
    if(input == "113")
    {
      Serial.println("3");
      state = 3;
    }
    if(input == "100")
    {
      Serial.println("4");
      state = 4;
    }
    if(input == "97")
    {
      Serial.println("5");
      state = 5;
    }
    if(input == "101")
    {
      Serial.println("6");
      state = 6;
    }
    if(input == "99")
    {
      Serial.println("7");
      state = 7;
    }
    if(input == "10")
    {
      //Serial.println("Nul Case");
    }  
  }


  switch(state) 
  {
    case 1: //z
      Serial.println("front");
      front();
      break;
    case 2: //s
      Serial.println("rear"); 
      rear();
      break;
    case 3: //q
      Serial.println("left"); 
      left();
      break;
    case 4: //d
      Serial.println("right");
      right();
      break;
    case 5: //a
      Serial.println("rotLeft");
      rotLeft(); 
      break;
    case 6: //e 
      rotRight(); 
      Serial.println("rotRight");
      break;
    case 7: //c
      Serial.println("stop");
      stop();
      break;
    default: 
      Serial.println("default");
      stop();
  }

}

void front()
{
  digitalWrite(frontRight_dirPin, HIGH);
  digitalWrite(frontLeft_dirPin, LOW);
  digitalWrite(rearRight_dirPin, HIGH);
  digitalWrite(rearLeft_dirPin, LOW);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}

void rear()
{
  digitalWrite(frontRight_dirPin, LOW);
  digitalWrite(frontLeft_dirPin, HIGH);
  digitalWrite(rearRight_dirPin, LOW);
  digitalWrite(rearLeft_dirPin, HIGH);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}


void left()
{
  digitalWrite(frontRight_dirPin, HIGH);
  digitalWrite(frontLeft_dirPin, HIGH);
  digitalWrite(rearRight_dirPin, LOW);
  digitalWrite(rearLeft_dirPin, LOW);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}

void right()
{
  digitalWrite(frontRight_dirPin, LOW);
  digitalWrite(frontLeft_dirPin, LOW);
  digitalWrite(rearRight_dirPin, HIGH);
  digitalWrite(rearLeft_dirPin, HIGH);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}

void rotLeft()
{
  digitalWrite(frontRight_dirPin, HIGH);
  digitalWrite(frontLeft_dirPin, HIGH);
  digitalWrite(rearRight_dirPin, HIGH);
  digitalWrite(rearLeft_dirPin, HIGH);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}

void rotRight()
{
  digitalWrite(frontRight_dirPin, LOW);
  digitalWrite(frontLeft_dirPin, LOW);
  digitalWrite(rearRight_dirPin, LOW);
  digitalWrite(rearLeft_dirPin, LOW);
  
  digitalWrite(frontRight_stepPin, HIGH);
  digitalWrite(frontLeft_stepPin, HIGH);
  digitalWrite(rearRight_stepPin, HIGH);
  digitalWrite(rearLeft_stepPin, HIGH);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}

void stop()
{
  digitalWrite(frontRight_dirPin, LOW);
  digitalWrite(frontLeft_dirPin, LOW);
  digitalWrite(rearRight_dirPin, LOW);
  digitalWrite(rearLeft_dirPin, LOW);
  
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
  digitalWrite(frontRight_stepPin, LOW);
  digitalWrite(frontLeft_stepPin, LOW);
  digitalWrite(rearRight_stepPin, LOW);
  digitalWrite(rearLeft_stepPin, LOW);
  delayMicroseconds(timing);
}
