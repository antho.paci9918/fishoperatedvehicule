import cv2
import numpy as np

# cap= cv2.VideoCapture(0)
cap = cv2.VideoCapture('assets/f.mp4')
x_medium = 0
y_medium = 0

x_repere = 0
y_repere = 0

while True:
    _,  frame = cap.read()
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    width = int(cap.get(3))
    height = int(cap.get(4))

    # red color
    low_red  = np.array([90, 50, 50]) # 161, 155, 84
    high_red = np.array([130, 255, 255]) # 179, 255 ,255
    mask = cv2.inRange(hsv_frame, low_red, high_red)
    red_mask = 255-mask

    contours, _ = cv2.findContours(red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=lambda x:cv2.contourArea(x), reverse=True)

    for cnt in contours:
        (x, y, w, h) = cv2.boundingRect(cnt)

        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 0), 2)
        x_medium = int((x + x + w) / 2)
        y_medium = int((y + y + h) / 2)
        x_repere = x_medium - int(width/2)
        y_repere = y_medium - int(height/2)
        break

    cv2.line(frame, (x_medium, 0), (x_medium, height), (0, 0, 0), 1)
    cv2.line(frame, (0, y_medium), (width, y_medium), (0, 0, 0), 1)
    cv2.imshow("Frame", frame)
    cv2.imshow("mask", red_mask)

    font = cv2.FONT_HERSHEY_SIMPLEX
    textColor = (0,0,0)
    textSize = 0.5

    cv2.line(frame, (int(width/2), 0), (int(width/2), height), (0, 0, 0), 1)
    cv2.line(frame, (0, int(height/2)), (width, int(height/2)), (0, 0, 0), 1)

    cv2.putText(frame,"X = ", (0,30), font, textSize, textColor, 1, cv2.LINE_AA)
    cv2.putText(frame,str(int(x_repere)), (40,30), font, textSize, textColor, 1, cv2.LINE_AA)
    cv2.putText(frame,"Y = ", (100,30), font, textSize, textColor, 1, cv2.LINE_AA)
    cv2.putText(frame,str(int(y_repere)), (140,30), font, textSize, textColor, 1, cv2.LINE_AA)

    cv2.imshow("Frame", frame)


    key = cv2.waitKey(1)

    if key == 27:
        break

cap.release()
cv2.destoyALLWindows()