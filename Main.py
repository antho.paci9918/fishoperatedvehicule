import cv2

img = cv2.imread('assets/fish.png',1) # Load image

# cv2.IMREAD_COLOR : default flag or -1 | Loads a color image. Any transparency of image will be neglected.
# cv2.IMREAD_GRAYSCALE : 0 | Loads image in grayscale mode
# cv2.Image_UNCHANGED : 1 | Loads image as such including alpha channel

#img = cv2.resize(img, (400,400))
img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)

cv2.imwrite('assets/newFish.png', img)

cv2.imshow('Image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()